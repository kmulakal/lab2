/**
 * kesav Mulakaluri
 * Chet Lemon
 */
package edu.ucsd.cs110w.temperature;
public class Celcius extends Temperature
{
	public Celcius(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: complete this shiet
		return getValue() + " C";
	}
	@Override
	public Temperature toCelcius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit((float) (getValue()*1.8+32));
	}
	public Temperature toKelvin()
	{
		return new Kelvin((float) (getValue()+273.15));
		
	}
}
