/**
 * Kesav Mulakaluri
 * Chet Lemon
 */
package edu.ucsd.cs110w.temperature;
public abstract class Temperature {

	float value;
	public Temperature(float v)
	{
		value = v;
	}
	public final float getValue()
	{
		return value;
	}
	public abstract Temperature toCelcius();
	public abstract Temperature toFahrenheit();
	public abstract Temperature toKelvin();
}
