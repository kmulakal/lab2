package edu.ucsd.cs110w.temperature;
// hello.
public class Kelvin extends Temperature{

	public Kelvin(float t) 
	{ 
	 super(t); 
	} 

	public String toString() 
	{ 
	 // TODO: Complete this method 
	 return getValue() + " K"; 
	} 
	@Override 
	public Temperature toCelcius() { 
	 // TODO: Complete this method 

	 return new Celcius((float) (getValue()-273.15)); 
	} 

	@Override 

	public Temperature toFahrenheit() {
	 // TODO: Complete this method 

	 return new Fahrenheit((float) ((getValue()-273.15)*1.8+32)); 
	}
	
	public Temperature toKelvin()
	{
		return this;
	}
}
