/**
 * Kesav Mulakaluri
 * Chet Lemon
 */
package edu.ucsd.cs110w.temperature;
public class Fahrenheit extends Temperature 
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO: this shit
		return getValue() + " F";
	}
	@Override
	public Temperature toCelcius() {
		// TODO Auto-generated method stub
		return new Celcius((float) ((float)(getValue()-32)/1.8));
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
	public Temperature toKelvin()
	{
		return new Kelvin((float) ((getValue()-32)*1.8+273.15));
	}
}
